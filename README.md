# README

## mva-framework-examples

Example programs and templates for the MVA Framework

***Written in LabVIEW 2020***

---
# Repo Contents

### Template

*A simple starting point that lets you QuickDrop and browse the relevant libraries*

* Palette maintenance is tedious and frustrating, and who really uses them anyway?
* Solution! -- Include the interfacey parts of the framework in your project and go to town.
* That's all this template provides.

### Simple Sequencer Example

*A great place to start for newbies and seasoned Actor Framework programmers alike*

* Multi-step example that shows the gradual progression of functionality in a new MVA application
* A few bookmarks to highlight interesting spots
* Rigorous commenting to help you find your way
* Each step is runnable out of the box (if framework and dependencies are installed)
* Replicates a reasonable (if simplified) implementation workflow
	1. Build UIs first (great when UI requirements exist!)
	2. Start publishing basic data and connecting to UIs
	3. Handle some UI value updates, like button presses
	4. Refactor Model(s) and Viewables as functionality grows
	5. Apply view layering/design patterns to manage viewable state

---
## How do I get set up?

* Install the framework and its dependencies using git (submodules)
* Pull the example source code from this repo and start running!

## Who do I talk to?

If you want help understanding, have implementation questions, or have a tough problem you're trying to solve, drop me a line and let's chat.

* Ethan Stern | Composed Systems, LLC
* ethan.stern@composed.io
* www.composedsystems.com